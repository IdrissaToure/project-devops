# Introduction

## Introduction générale

Dans le cadre de fin des cours DevOps, nous avons été amener a réaliser un projet basé sur la matière ! Ce dernier couvre en générale tous les points essentiels de cette approche qu’est DevOps.
DevOps

Le DevOps, est une approche permettant de réunir deux fonctions clés de la DSI d’une entreprise chargée du développement d’applications. D’un côté, vous avez terme le “Dev” représentant les équipes de développeurs et de l’autre, vous avez le terme “Ops” représentant les équipes d’exploitation systèmes (operations en anglais). Il peut être également interprété d’une autre façon, en se penchant sur une approche plutôt « métier » où le Devops est un poste qu’occupe un salarié ayant des compétences dans le développement, mais aussi en ingénierie système. Cette approche DevOps, souvent mal comprise, repose sur la méthode agile et du lean management où les responsables métiers, le développement, les opérations et le service qualité collaborent pour déployer en continu les différentes versions ou fonctionnalités d’un logiciel. L’objectif étant de s’adapter aux opportunités du marché et aux retours clients.


## CONTEXT DU PROJET

### Aperçu du projet

KATA est une jeune petite entreprise de développement de solutions et de services logiciels. En plus de ses deux co-fondateurs, elle est constituée de deux commerciaux, un marketeur, un comptable, un designer, une dizaine de développeurs (dont deux ingénieurs QA) et quatre ingénieurs d’exploitation qui gèrent une infrastructure “on premise”. Généralement, les solutions développées par cette entreprise sont de bonne qualité, mais la fréquence de leur livraison/déploiement est un peu lente par rapport à leurs concurrents. D’autre part, les équipes de l’entreprise trouvent du mal à intégrer les changements fréquents des besoins exprimés par leurs clients. Pour les quelques mois à venir, KATA veut saisir une nouvelle opportunité du marché, et compte se focaliser sur le développement d’une nouvelle solution avec une idée disruptive. Visant a augmenter la productivité de leurs tout en rendant leur processus de livraison logicielle plus rapide, fiable et sécurisé je fut charger en tant que stagiaire su sein de l’entreprise de réaliser un PoC (Proof of Concept) leur permettant de découvrir et d’évaluer l’efficacité de l’approche DevSecOps par rapport à ces trois volets People, Process et Tools.
### Les livrables

Pour ce qui est du livrable il était question de développer une application web nommée Mcommerce avec le framework Spring Boot (Java) sachant que cette application est composée d’une partie client ainsi que trois microservices à savoir(voir la figure ci-après pour avoir une idée sur les fonctionnalités proposées ainsi que la communication entre microservices) :

    Produit
    Commande
    Paiement

fig_2

Après réalisation l’application seras héberger sur un cluster Kubernetes a travers un CI/CD pipeline (voir la figure ci-après pour illustration)

devops_by_enoch
### TOPOLOGIE
#### Choix

Après une longue réflexion, sachant qu’au paravent l’entreprise n’a jamais toucher ni au DevOps ni aux différents outils, j’ai jugé nécessaire de proposé la première topologie c’est-à-dire le ‘’type 1’’ (voir la figure ci-après pour illustration)

topology
#### Avantage

Cette dernière mise sur la collaboration entre Dev et Ops, sans oublier qu’il demande une forte technique de leadership. Surnommé comme ‘’terre promise’’ de DevOps, la fluidité de la collaboration, chacune des équipes se spécialisent en cas de, mais partage également le cas échéant. Les avantages sont comme suite :

    Une meilleure coordination
    Objectif commun et clair
    Surtout pour une entreprise qui débute ce serais le meilleur moyen de ne pas
    tomber dans le piège d’un anti-type
